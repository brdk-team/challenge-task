RUN SERVICE
-----------
`docker-compose up`  

API Endpoints
-------------
1. `/plants` - the CRUD endpoint to manage Plants.  
To create new plant a `name` must be provided in the POST request

2. `/datapoints/update` - updates plant's data. POST request is allowed only.  
Required params:  
 - `plant-id` - an id of plant instance  
 - `from` - a date in "YYYY-mm-dd" format  
 - `to` - a date in "YYYY-mm-dd" format  
 
3. `/datapoints/report` - generates the report for specified plant. GET request is allowed only.  
Required params:
 - `plant-id` - an id of plant instance  
 - `from` - a date in "YYYY-mm-dd" format  
 - `to` - a date in "YYYY-mm-dd" format  
Optional params:
 - `hour-from` - an hour as integer between 0 and 24  
 - `hour-to` - an hour as integer between 0 and 24  
 If optional params are provided data in range of specified hour will be filtered.  
 For example, if `plant-id=1&from=2020-01-01&to=2020-02-01&hour-from=12&hour-to=15` is requested,
 result will contain data between 12:00 and 15:00 for every day from the 1st of January to the 1st of February
 