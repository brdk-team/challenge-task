from rest_framework import serializers

from backend.models import Plant


class PlantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plant
        fields = ['id', 'name']


class DataPointReportSerializer(serializers.Serializer):
    date = serializers.DateField()
    energy_expected_sum = serializers.FloatField()
    energy_expected_avg = serializers.FloatField()
    energy_expected_min = serializers.FloatField()
    energy_expected_max = serializers.FloatField()
    energy_observed_sum = serializers.FloatField()
    energy_observed_avg = serializers.FloatField()
    energy_observed_min = serializers.FloatField()
    energy_observed_max = serializers.FloatField()
    irradiation_expected_sum = serializers.FloatField()
    irradiation_expected_avg = serializers.FloatField()
    irradiation_expected_min = serializers.FloatField()
    irradiation_expected_max = serializers.FloatField()
    irradiation_observed_sum = serializers.FloatField()
    irradiation_observed_avg = serializers.FloatField()
    irradiation_observed_min = serializers.FloatField()
    irradiation_observed_max = serializers.FloatField()
