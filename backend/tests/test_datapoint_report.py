import datetime
from random import uniform

import pytz
from django.test import TestCase

from backend.datapoint.filters import DataPointFilter
from backend.datapoint.report import DataPointReport
from backend.models import Plant, DataPoint


class DataPointReportTest(TestCase):

    def setUp(self):
        super(DataPointReportTest, self).setUp()
        plant = Plant.objects.create(name='test')
        self.plant = plant
        self.dp1 = DataPoint.objects.create(plant_id=plant.id,
                                            datetime=datetime.datetime(2019, 1, 1, 0, 0, 0,tzinfo=pytz.UTC),
                                            energy_expected=uniform(0, 150), energy_observed=uniform(0, 150),
                                            irradiation_expected=uniform(0, 150), irradiation_observed=uniform(0, 150))
        self.dp2 = DataPoint.objects.create(plant_id=plant.id,
                                            datetime=datetime.datetime(2019, 1, 1, 1, 0, 0, tzinfo=pytz.UTC),
                                            energy_expected=uniform(0, 150), energy_observed=uniform(0, 150),
                                            irradiation_expected=uniform(0, 150), irradiation_observed=uniform(0, 150))
        self.dp3 = DataPoint.objects.create(plant_id=plant.id,
                                            datetime=datetime.datetime(2019, 1, 1, 2, 0, 0, tzinfo=pytz.UTC),
                                            energy_expected=uniform(0, 150), energy_observed=uniform(0, 150),
                                            irradiation_expected=uniform(0, 150), irradiation_observed=uniform(0, 150))
        self.dp4 = DataPoint.objects.create(plant_id=plant.id,
                                            datetime=datetime.datetime(2019, 1, 1, 3, 0, 0, tzinfo=pytz.UTC),
                                            energy_expected=uniform(0, 150), energy_observed=uniform(0, 150),
                                            irradiation_expected=uniform(0, 150), irradiation_observed=uniform(0, 150))
        self.dp5 = DataPoint.objects.create(plant_id=plant.id,
                                            datetime=datetime.datetime(2019, 1, 2, 0, 0, 0, tzinfo=pytz.UTC),
                                            energy_expected=uniform(0, 150), energy_observed=uniform(0, 150),
                                            irradiation_expected=uniform(0, 150), irradiation_observed=uniform(0, 150))
        self.dp6 = DataPoint.objects.create(plant_id=plant.id,
                                            datetime=datetime.datetime(2019, 1, 2, 1, 0, 0, tzinfo=pytz.UTC),
                                            energy_expected=uniform(0, 150), energy_observed=uniform(0, 150),
                                            irradiation_expected=uniform(0, 150), irradiation_observed=uniform(0, 150))
        self.dp7 = DataPoint.objects.create(plant_id=plant.id,
                                            datetime=datetime.datetime(2019, 1, 2, 2, 0, 0, tzinfo=pytz.UTC),
                                            energy_expected=uniform(0, 150), energy_observed=uniform(0, 150),
                                            irradiation_expected=uniform(0, 150), irradiation_observed=uniform(0, 150))
        self.dp8 = DataPoint.objects.create(plant_id=plant.id,
                                            datetime=datetime.datetime(2019, 1, 2, 3, 0, 0, tzinfo=pytz.UTC),
                                            energy_expected=uniform(0, 150), energy_observed=uniform(0, 150),
                                            irradiation_expected=uniform(0, 150), irradiation_observed=uniform(0, 150))

    def tearDown(self):
        super().tearDown()
        Plant.objects.all().delete()
        DataPoint.objects.all().delete()

    def test_generate_all(self):
        datapoints = [self.dp1, self.dp2, self.dp3, self.dp4, self.dp5, self.dp6, self.dp7, self.dp8]
        datapoints_on_20190101 = [self.dp1, self.dp2, self.dp3, self.dp4]
        datapoints_on_20190102 = [self.dp5, self.dp6, self.dp7, self.dp8]

        fltr = DataPointFilter(self.plant.id, datetime.datetime(2019, 1, 1, 0, 0, 0, tzinfo=pytz.UTC),
                               datetime.datetime(2019, 1, 3, 0, 0, 0, tzinfo=pytz.UTC))
        report = DataPointReport(fltr)
        data = report.generate()

        expected = self._get_expected(datapoints)
        self._assert_values(data['summary'], expected)

        expected = self._get_expected(datapoints_on_20190101)
        self._assert_values(data['result'][0], expected)

        expected = self._get_expected(datapoints_on_20190102)
        self._assert_values(data['result'][1], expected)

    def test_generate_one_day(self):
        datapoints_on_20190101 = [self.dp1, self.dp2, self.dp3, self.dp4]

        fltr = DataPointFilter(self.plant.id, datetime.datetime(2019, 1, 1, 0, 0, 0, tzinfo=pytz.UTC),
                               datetime.datetime(2019, 1, 2, 0, 0, 0, tzinfo=pytz.UTC))
        report = DataPointReport(fltr)
        data = report.generate()

        expected = self._get_expected(datapoints_on_20190101)
        self._assert_values(data['summary'], expected)

        expected = self._get_expected(datapoints_on_20190101)
        self._assert_values(data['result'][0], expected)

    def test_generate_all_with_hour_range(self):
        datapoints = [self.dp2, self.dp3, self.dp6, self.dp7]
        datapoints_on_20190101 = [self.dp2, self.dp3]
        datapoints_on_20190102 = [self.dp6, self.dp7]

        fltr = DataPointFilter(self.plant.id, datetime.datetime(2019, 1, 1, 0, 0, 0, tzinfo=pytz.UTC),
                               datetime.datetime(2019, 1, 3, 0, 0, 0, tzinfo=pytz.UTC), 1, 2)
        report = DataPointReport(fltr)
        data = report.generate()

        expected = self._get_expected(datapoints)
        self._assert_values(data['summary'], expected)

        expected = self._get_expected(datapoints_on_20190101)
        self._assert_values(data['result'][0], expected)

        expected = self._get_expected(datapoints_on_20190102)
        self._assert_values(data['result'][1], expected)

    def _assert_values(self, values, expected):
        self.assertAlmostEqual(values['energy_expected_sum'], expected['energy_expected_sum'], places=10)
        self.assertAlmostEqual(values['energy_expected_avg'], expected['energy_expected_avg'], places=10)
        self.assertAlmostEqual(values['energy_expected_min'], expected['energy_expected_min'], places=10)
        self.assertAlmostEqual(values['energy_expected_max'], expected['energy_expected_max'], places=10)
        self.assertAlmostEqual(values['energy_observed_sum'], expected['energy_observed_sum'], places=10)
        self.assertAlmostEqual(values['energy_observed_avg'], expected['energy_observed_avg'], places=10)
        self.assertAlmostEqual(values['energy_observed_min'], expected['energy_observed_min'], places=10)
        self.assertAlmostEqual(values['energy_observed_max'], expected['energy_observed_max'], places=10)
        self.assertAlmostEqual(values['irradiation_expected_sum'], expected['irradiation_expected_sum'], places=10)
        self.assertAlmostEqual(values['irradiation_expected_avg'], expected['irradiation_expected_avg'], places=10)
        self.assertAlmostEqual(values['irradiation_expected_min'], expected['irradiation_expected_min'], places=10)
        self.assertAlmostEqual(values['irradiation_expected_max'], expected['irradiation_expected_max'], places=10)
        self.assertAlmostEqual(values['irradiation_observed_sum'], expected['irradiation_observed_sum'], places=10)
        self.assertAlmostEqual(values['irradiation_observed_avg'], expected['irradiation_observed_avg'], places=10)
        self.assertAlmostEqual(values['irradiation_observed_min'], expected['irradiation_observed_min'], places=10)
        self.assertAlmostEqual(values['irradiation_observed_max'], expected['irradiation_observed_max'], places=10)

    def _get_expected(self, datapoints):
        return {
            'energy_expected_sum': sum([float(dp.energy_expected) for dp in datapoints]),
            'energy_expected_avg': sum([float(dp.energy_expected) for dp in datapoints]) / len(datapoints),
            'energy_expected_min': min([float(dp.energy_expected) for dp in datapoints]),
            'energy_expected_max': max([float(dp.energy_expected) for dp in datapoints]),
            'energy_observed_sum': sum([float(dp.energy_observed) for dp in datapoints]),
            'energy_observed_avg': sum([float(dp.energy_observed) for dp in datapoints]) / len(datapoints),
            'energy_observed_min': min([float(dp.energy_observed) for dp in datapoints]),
            'energy_observed_max': max([float(dp.energy_observed) for dp in datapoints]),
            'irradiation_expected_sum': sum([float(dp.irradiation_expected) for dp in datapoints]),
            'irradiation_expected_avg': sum([float(dp.irradiation_expected) for dp in datapoints]) / len(datapoints),
            'irradiation_expected_min': min([float(dp.irradiation_expected) for dp in datapoints]),
            'irradiation_expected_max': max([float(dp.irradiation_expected) for dp in datapoints]),
            'irradiation_observed_sum': sum([float(dp.irradiation_observed) for dp in datapoints]),
            'irradiation_observed_avg': sum([float(dp.irradiation_observed) for dp in datapoints]) / len(datapoints),
            'irradiation_observed_min': min([float(dp.irradiation_observed) for dp in datapoints]),
            'irradiation_observed_max': max([float(dp.irradiation_observed) for dp in datapoints]),
        }
