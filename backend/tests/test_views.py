from unittest.mock import patch, Mock

from django.core.exceptions import ValidationError
from django.test import TestCase, Client
from django.urls import reverse


class DataPointReportViewTest(TestCase):
    def setUp(self):
        super(DataPointReportViewTest, self).setUp()
        self.client = Client()
        self.url = reverse('datapoints-report')

    @patch('backend.datapoint.report.DataPointFilter.from_request')
    @patch('backend.datapoint.report.DataPointReport.generate')
    def test_exception(self, m_generate, m_from_request):
        def side_effect(*args, **kwargs):
            raise ValueError

        m_from_request.return_value = Mock()
        m_generate.side_effect = side_effect
        r = self.client.get(self.url)
        self.assertTrue(m_generate.called)
        self.assertEqual(r.status_code, 500)

    @patch('backend.datapoint.report.DataPointFilter.from_request')
    def test_wrong_param(self, m_from_request):
        def side_effect(*args, **kwargs):
            raise ValidationError('bad request')

        m_from_request.side_effect = side_effect
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, 400)

    @patch('backend.datapoint.report.DataPointFilter.from_request')
    @patch('backend.datapoint.report.DataPointReport.generate')
    def test_response(self, m_generate, m_from_request):
        m_from_request.return_value = Mock()
        m_generate.return_value = {}
        r = self.client.get(self.url)
        self.assertTrue(m_generate.called)
        self.assertEqual(r.status_code, 200)

    def test_wrong_request(self):
        r = self.client.post(self.url)
        self.assertEqual(r.status_code, 405)


class DataPointUpdateViewTest(TestCase):
    def setUp(self):
        super(DataPointUpdateViewTest, self).setUp()
        self.client = Client()
        self.url = reverse('datapoints-update')

    @patch('backend.datapoint.report.DataPointFilter.from_request')
    @patch('backend.datapoint.updater.DataPointUpdater.update')
    def test_exception(self, m_update, m_from_request):
        def side_effect(*args, **kwargs):
            raise ValueError

        m_from_request.return_value = Mock()
        m_update.side_effect = side_effect
        r = self.client.post(self.url)
        self.assertTrue(m_update.called)
        self.assertEqual(r.status_code, 500)

    @patch('backend.datapoint.report.DataPointFilter.from_request')
    def test_wrong_param(self, m_from_request):
        def side_effect(*args, **kwargs):
            raise ValidationError('bad request')

        m_from_request.side_effect = side_effect
        r = self.client.post(self.url)
        self.assertEqual(r.status_code, 400)

    @patch('backend.datapoint.report.DataPointFilter.from_request')
    @patch('backend.datapoint.updater.DataPointUpdater.update')
    def test_response(self, m_update, m_from_request):
        m_from_request.return_value = Mock()
        r = self.client.post(self.url)
        self.assertTrue(m_update.called)
        self.assertEqual(r.status_code, 200)

    def test_wrong_request(self):
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, 405)
