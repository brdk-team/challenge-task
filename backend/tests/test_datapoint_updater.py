import datetime
from unittest.mock import patch

import pytz
from django.test import TestCase

from backend.datapoint.filters import DataPointFilter
from backend.datapoint.updater import DataPointFetchException, DataPointUpdater
from backend.models import Plant, DataPoint


class DataPointUpdaterTest(TestCase):

    def setUp(self):
        super(DataPointUpdaterTest, self).setUp()
        self.plant = Plant.objects.create(name='test')
        self.filter = DataPointFilter(self.plant.id, datetime.datetime(2019, 1, 1), datetime.datetime(2019, 2, 1))

    def tearDown(self):
        super().tearDown()
        Plant.objects.all().delete()
        DataPoint.objects.all().delete()

    @patch('backend.datapoint.updater.DataPointUpdater.fetch_data')
    def test_fetch_data_raises(self, m_fetch_data):
        def side_effect(*args, **kwargs):
            raise DataPointFetchException('no data')

        m_fetch_data.side_effect = side_effect
        updater = DataPointUpdater(self.filter)
        with self.assertRaises(DataPointFetchException):
            updater.update()

    @patch('backend.datapoint.updater.DataPointUpdater.fetch_data')
    def test_create_new(self, m_fetch_data):
        data = [
            {
                'datetime': '2019-01-01T00:00:00',
                'expected': {
                    'energy': 87.55317774223157,
                    'irradiation': 98.19878838432548
                },
                'observed': {
                    'energy': 90.78559770167864,
                    'irradiation': 30.085498370965905
                }
            },
            {
                'datetime': '2019-01-01T01:00:00',
                'expected': {
                    'energy': 87.55317774223157,
                    'irradiation': 98.19878838432548
                },
                'observed': {
                    'energy': 90.78559770167864,
                    'irradiation': 30.085498370965905
                }
            }
        ]

        m_fetch_data.return_value = data
        updater = DataPointUpdater(self.filter)
        updater.update()
        self.assertEqual(DataPoint.objects.count(), 2)

    @patch('backend.datapoint.updater.DataPointUpdater.fetch_data')
    def test_create_and_update(self, m_fetch_data):
        datapoint = DataPoint.objects.create(
            plant_id=self.plant.id,
            datetime=datetime.datetime(2019, 1, 1, 1, 0, 0, tzinfo=pytz.UTC),
            energy_expected=1,
            energy_observed=2,
            irradiation_expected=3,
            irradiation_observed=4
        )
        data = [
            {
                'datetime': '2019-01-01T00:00:00',
                'expected': {
                    'energy': 87.55317774223157,
                    'irradiation': 98.19878838432548
                },
                'observed': {
                    'energy': 90.78559770167864,
                    'irradiation': 30.085498370965905
                }
            },
            {
                'datetime': '2019-01-01T01:00:00',
                'expected': {
                    'energy': 187.55317774223157,
                    'irradiation': 198.19878838432548
                },
                'observed': {
                    'energy': 190.78559770167864,
                    'irradiation': 130.085498370965905
                }
            }
        ]

        m_fetch_data.return_value = data
        updater = DataPointUpdater(self.filter)
        updater.update()
        self.assertEqual(DataPoint.objects.count(), 2)

        datapoint.refresh_from_db()
        print(datapoint.energy_expected)
        self.assertEqual(datapoint.energy_expected, str(data[1]['expected']['energy']))
        self.assertEqual(datapoint.energy_observed, str(data[1]['observed']['energy']))
        self.assertEqual(datapoint.irradiation_expected, str(data[1]['expected']['irradiation']))
        self.assertEqual(datapoint.irradiation_observed, str(data[1]['observed']['irradiation']))

    @patch('backend.datapoint.updater.DataPointUpdater.fetch_data')
    def test_update_unexpected_data(self, m_fetch_data):
        data = [{
            'datetime': '2019-01-01T00:00:00',
            'expected_energy': 87.55317774223157,
            'expected_irradiation': 98.19878838432548,
            'observed_energy': 90.78559770167864,
            'observed_irradiation': 30.085498370965905
        }]

        m_fetch_data.return_value = data
        updater = DataPointUpdater(self.filter)
        updater.update()
        self.assertEqual(DataPoint.objects.count(), 0)
