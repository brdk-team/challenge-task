import datetime

import pytz
from django.core.exceptions import ValidationError
from django.http import QueryDict
from django.test import TestCase

from backend.datapoint.filters import DataPointFilter


class DataPointFilterTest(TestCase):

    def setUp(self):
        super(DataPointFilterTest, self).setUp()
        self.query = {
            'plant-id': 1,
            'from': '2019-01-01',
            'to': '2019-02-01',
            'hour-from': '12',
            'hour-to': '16',
        }

    def test_full(self):
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.plant_id, 1)
        self.assertEqual(fltr.date_from, datetime.datetime(2019, 1, 1, tzinfo=pytz.UTC))
        self.assertEqual(fltr.date_to, datetime.datetime(2019, 2, 1, tzinfo=pytz.UTC))
        self.assertEqual(fltr.hour_from, 12)
        self.assertEqual(fltr.hour_to, 16)

    def test_without_hours(self):
        del self.query['hour-from']
        del self.query['hour-to']
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.plant_id, 1)
        self.assertEqual(fltr.date_from, datetime.datetime(2019, 1, 1, tzinfo=pytz.UTC))
        self.assertEqual(fltr.date_to, datetime.datetime(2019, 2, 1, tzinfo=pytz.UTC))
        self.assertEqual(fltr.hour_from, None)
        self.assertEqual(fltr.hour_to, None)

    def test_clean_plant_id(self):
        del self.query['plant-id']
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'plant-id field is not valid, expected int'):
            DataPointFilter.from_request(query_dict)

    def test_clean_date_from(self):
        del self.query['from']
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'date from or date to field is not valid'):
            DataPointFilter.from_request(query_dict)

        self.query['from'] = '20190101'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'date from or date to field is not valid'):
            DataPointFilter.from_request(query_dict)

        self.query['from'] = '2019-01-01T04:20:00'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'date from or date to field is not valid'):
            DataPointFilter.from_request(query_dict)

        self.query['from'] = '2019-20-01'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'date from or date to field is not valid'):
            DataPointFilter.from_request(query_dict)

    def test_clean_date_to(self):
        del self.query['to']
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'date from or date to field is not valid'):
            DataPointFilter.from_request(query_dict)

        self.query['to'] = '20190101'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'date from or date to field is not valid'):
            DataPointFilter.from_request(query_dict)

        self.query['to'] = '2019-01-01T04:20:00'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'date from or date to field is not valid'):
            DataPointFilter.from_request(query_dict)

        self.query['to'] = '2019-20-01'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'date from or date to field is not valid'):
            DataPointFilter.from_request(query_dict)

    def test_clean_hour_from(self):
        del self.query['hour-from']
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 0)
        self.assertEqual(fltr.hour_to, 16)

        self.query['hour-from'] = ''
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 0)

        self.query['hour-from'] = '20'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 16)
        self.assertEqual(fltr.hour_to, 20)

        self.query['hour-from'] = 'twelve'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'hour should be between 0 and 24'):
            DataPointFilter.from_request(query_dict)

        self.query['hour-from'] = '0'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 0)

        self.query['hour-from'] = '24'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 16)
        self.assertEqual(fltr.hour_to, 24)

        self.query['hour-from'] = '25'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'hour should be between 0 and 24'):
            DataPointFilter.from_request(query_dict)

    def test_clean_hour_to(self):
        del self.query['hour-to']
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 12)
        self.assertEqual(fltr.hour_to, 24)

        self.query['hour-to'] = ''
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 12)
        self.assertEqual(fltr.hour_to, 24)

        self.query['hour-to'] = '2'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 2)
        self.assertEqual(fltr.hour_to, 12)

        self.query['hour-to'] = 'twelve'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'hour should be between 0 and 24'):
            DataPointFilter.from_request(query_dict)

        self.query['hour-to'] = '0'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 0)

        self.query['hour-to'] = '24'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        fltr = DataPointFilter.from_request(query_dict)
        self.assertEqual(fltr.hour_from, 12)
        self.assertEqual(fltr.hour_to, 24)

        self.query['hour-to'] = '25'
        query_dict = QueryDict('', mutable=True)
        query_dict.update(self.query)
        with self.assertRaisesMessage(ValidationError, 'hour should be between 0 and 24'):
            DataPointFilter.from_request(query_dict)
