from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_GET
from rest_framework import viewsets

from backend.datapoint.filters import DataPointFilter
from backend.datapoint.report import DataPointReport
from backend.models import Plant
from backend.serializers import PlantSerializer
from backend.datapoint.updater import DataPointUpdater


class PlantViewSet(viewsets.ModelViewSet):
    queryset = Plant.objects.all()
    serializer_class = PlantSerializer


@require_POST
@csrf_exempt
def update_datapoint(request):
    try:
        datapoint_filter = DataPointFilter.from_request(request.POST)
        datapoint_updater = DataPointUpdater(datapoint_filter)
        datapoint_updater.update()
        return JsonResponse({'detail': 'OK'})
    except ValidationError as e:
        return JsonResponse({'detail': e.message}, status=400)
    except Exception:
        return JsonResponse({'detail': 'something went wrong'}, status=500)

@require_GET
def datapoint_report(request):
    try:
        datapoint_filter = DataPointFilter.from_request(request.GET)
        report = DataPointReport(datapoint_filter)
        result = report.generate()
        return JsonResponse(result)
    except ValidationError as e:
        return JsonResponse({'detail': e.message}, status=400)
    except Exception:
        return JsonResponse({'detail': 'something went wrong'}, status=500)
