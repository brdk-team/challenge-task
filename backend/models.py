from django.db import models


class Plant(models.Model):
    name = models.CharField(max_length=20)


class DataPoint(models.Model):
    id = models.BigAutoField(primary_key=True)
    plant = models.ForeignKey(Plant, models.CASCADE, related_name='datapoints')
    datetime = models.DateTimeField()
    energy_expected = models.CharField(max_length=30)
    energy_observed = models.CharField(max_length=30)
    irradiation_expected = models.CharField(max_length=30)
    irradiation_observed = models.CharField(max_length=30)

    class Meta:
        unique_together = [
            ('plant', 'datetime')
        ]
