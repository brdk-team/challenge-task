import datetime
import logging

import requests
from django.conf import settings
from django.utils import timezone
from requests import HTTPError

from backend.datapoint.filters import DataPointFilter
from backend.models import Plant, DataPoint

log = logging.getLogger(__name__)


class DataPointFetchException(Exception):
    def __init__(self, message, *args: object, **kwargs: object) -> None:
        super().__init__(*args, **kwargs)
        self.message = message


class DataPointUpdater:
    def __init__(self, query_filter: DataPointFilter):
        super().__init__()
        self.filter = query_filter
        self.monitoring_url = settings.MONITORING_URL

    @staticmethod
    def update_prev_date() -> None:
        today = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        yesterday = today - datetime.timedelta(days=1)
        for plant in Plant.objects.all():
            try:
                filter_ = DataPointFilter(plant.id, yesterday, today)
                updater = DataPointUpdater(filter_)
                updater.update()
            except Exception:
                log.exception(f'cannot update monitoring data for plant {plant.id}')

    def update(self) -> None:
        for obj in self.fetch_data():
            try:
                self._create_or_update_from_object(obj)
            except Exception:
                log.error(f'cannot create or update data for plant {self.filter.plant_id} from obj {obj}')

    def fetch_data(self) -> list:
        params = {
            'plant-id': self.filter.plant_id,
            'from': self.filter.date_from.strftime('%Y-%m-%d'),
            'to': self.filter.date_to.strftime('%Y-%m-%d')
        }
        try:
            r = requests.get(self.monitoring_url, params=params)
            r.raise_for_status()
            data = r.json()
            if 'error' in data:
                raise DataPointFetchException(data['error'])
            return data
        except HTTPError:
            message = f'cannot fetch data from {self.monitoring_url}'
            log.exception(message)
            raise DataPointFetchException(message)

    def _create_or_update_from_object(self, obj):
        dt = datetime.datetime.strptime(obj['datetime'], '%Y-%m-%dT%H:%M:%S')
        dt = timezone.make_aware(dt)
        try:
            data_point = DataPoint.objects.get(plant_id=self.filter.plant_id, datetime=dt)
        except DataPoint.DoesNotExist:
            data_point = DataPoint()
            data_point.plant_id = self.filter.plant_id
            data_point.datetime = dt

        data_point.energy_expected = str(obj['expected']['energy'])
        data_point.energy_observed = str(obj['observed']['energy'])
        data_point.irradiation_expected = str(obj['expected']['irradiation'])
        data_point.irradiation_observed = str(obj['observed']['irradiation'])
        data_point.save()
