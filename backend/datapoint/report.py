from django.db.models import Sum, DateField, Avg, Min, Max, QuerySet, FloatField
from django.db.models.functions import Cast

from backend.datapoint.filters import DataPointFilter
from backend.models import DataPoint
from backend.serializers import DataPointReportSerializer


class DataPointReport:

    def __init__(self, query_filter: DataPointFilter) -> None:
        super().__init__()
        self.filter = query_filter
        self.summary = {}

    def generate(self):
        queryset = self._get_queryset()
        serializer = DataPointReportSerializer(queryset, many=True)
        return {
            'result': list(serializer.data),
            'summary': self.summary
        }

    def _get_queryset(self) -> QuerySet:
        queryset = DataPoint.objects.filter(plant_id=self.filter.plant_id,
                                            datetime__gte=self.filter.date_from,
                                            datetime__lt=self.filter.date_to)

        if self.filter.hour_range:
            queryset = queryset.filter(datetime__hour__range=self.filter.hour_range)

        queryset = queryset.annotate(
            ee=Cast('energy_expected', FloatField()),
            eo=Cast('energy_observed', FloatField()),
            ie=Cast('irradiation_expected', FloatField()),
            io=Cast('irradiation_observed', FloatField()),
        )

        self.summary = queryset.aggregate(
            energy_expected_sum=Sum('ee'),
            energy_expected_avg=Avg('ee'),
            energy_expected_min=Min('ee'),
            energy_expected_max=Max('ee'),
            energy_observed_sum=Sum('eo'),
            energy_observed_avg=Avg('eo'),
            energy_observed_min=Min('eo'),
            energy_observed_max=Max('eo'),
            irradiation_expected_sum=Sum('ie'),
            irradiation_expected_avg=Avg('ie'),
            irradiation_expected_min=Min('ie'),
            irradiation_expected_max=Max('ie'),
            irradiation_observed_sum=Sum('io'),
            irradiation_observed_avg=Avg('io'),
            irradiation_observed_min=Min('io'),
            irradiation_observed_max=Max('io')
        )

        return (queryset.annotate(date=Cast('datetime', DateField()))
                        .values('date')
                        .order_by('date')
                        .annotate(
                            energy_expected_sum=Sum('ee'),
                            energy_expected_avg=Avg('ee'),
                            energy_expected_min=Min('ee'),
                            energy_expected_max=Max('ee'),
                            energy_observed_sum=Sum('eo'),
                            energy_observed_avg=Avg('eo'),
                            energy_observed_min=Min('eo'),
                            energy_observed_max=Max('eo'),
                            irradiation_expected_sum=Sum('ie'),
                            irradiation_expected_avg=Avg('ie'),
                            irradiation_expected_min=Min('ie'),
                            irradiation_expected_max=Max('ie'),
                            irradiation_observed_sum=Sum('io'),
                            irradiation_observed_avg=Avg('io'),
                            irradiation_observed_min=Min('io'),
                            irradiation_observed_max=Max('io'),))
