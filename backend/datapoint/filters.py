import datetime

from django.core.exceptions import ValidationError
from django.http import QueryDict
from django.utils.timezone import is_aware, make_aware


class DataPointFilter:
    def __init__(self, plant_id: int, date_from: datetime.datetime, date_to: datetime.datetime,
                 hour_from: int = None, hour_to: int = None) -> None:
        super().__init__()
        self.plant_id = plant_id
        self.date_from = make_aware(date_from) if not is_aware(date_from) else date_from
        self.date_to = make_aware(date_to) if not is_aware(date_to) else date_to
        if self.date_to < self.date_from:
            self.date_from, self.date_to = self.date_to, self.date_from

        if hour_from is not None and hour_to is not None:
            if hour_to < hour_from:
                hour_from, hour_to = hour_to, hour_from
        self.hour_from = hour_from
        self.hour_to = hour_to

    @classmethod
    def from_request(cls, query_dict: QueryDict):
        cleaned_query_dict = DataPointFilter.clean_post_params(query_dict)
        params = {
            'plant_id': cleaned_query_dict.get('plant-id'),
            'date_from': cleaned_query_dict.get('from'),
            'date_to': cleaned_query_dict.get('to'),
            'hour_from': cleaned_query_dict.get('hour-from'),
            'hour_to': cleaned_query_dict.get('hour-to'),
        }
        return cls(**params)

    @staticmethod
    def clean_post_params(query_dict: QueryDict) -> dict:
        plant_id = query_dict.get('plant-id')
        date_from = query_dict.get('from')
        date_to = query_dict.get('to')
        hour_from = query_dict.get('hour-from')
        hour_to = query_dict.get('hour-to')

        try:
            plant_id = int(plant_id)
        except (TypeError, ValueError):
            raise ValidationError('plant-id field is not valid, expected int')

        def clean_date(date):
            date_format = '%Y-%m-%d'
            try:
                return datetime.datetime.strptime(date, date_format)
            except (TypeError, ValueError):
                raise ValidationError('date from or date to field is not valid, expected format is "YYYY-mm-dd"')

        def clean_hour(hour):
            if not hour:
                return
            try:
                hour = int(hour)
                if not 0 <= hour < 25:
                    raise ValueError
                return hour
            except ValueError:
                raise ValidationError('hour should be between 0 and 24')

        cleaned_post_params = {
            'plant-id': plant_id,
            'from': clean_date(date_from),
            'to': clean_date(date_to)
        }

        if hour_from and hour_to:
            cleaned_post_params['hour-from'] = clean_hour(hour_from)
            cleaned_post_params['hour-to'] = clean_hour(hour_to)
        elif not hour_from and hour_to:
            cleaned_post_params['hour-from'] = 0
            cleaned_post_params['hour-to'] = clean_hour(hour_to)
        elif hour_from and not hour_to:
            cleaned_post_params['hour-from'] = clean_hour(hour_from)
            cleaned_post_params['hour-to'] = 24

        return cleaned_post_params

    def is_hour_range_existed(self):
        return self.hour_from is not None and self.hour_to is not None

    @property
    def hour_range(self):
        if self.is_hour_range_existed():
            return [self.hour_from, self.hour_to]
