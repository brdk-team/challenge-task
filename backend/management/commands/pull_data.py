from django.core.management.base import BaseCommand

from backend.datapoint.updater import DataPointUpdater


class Command(BaseCommand):
    help = 'pull prev date data from the monitoring service'

    def handle(self, *args, **options):
        DataPointUpdater.update_prev_date()
