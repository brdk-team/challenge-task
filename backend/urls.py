from django.urls import include, path
from rest_framework import routers

from backend.views import PlantViewSet, update_datapoint, datapoint_report

router = routers.DefaultRouter()
router.register(r'plants', PlantViewSet)

urlpatterns = [
    path('datapoints/update', update_datapoint, name='datapoints-update'),
    path('datapoints/report', datapoint_report, name='datapoints-report'),
    path('', include(router.urls), name='backend'),
]
